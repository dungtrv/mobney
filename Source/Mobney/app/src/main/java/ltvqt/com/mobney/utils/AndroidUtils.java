package ltvqt.com.mobney.utils;

import android.content.Context;

/**
 * Created by hoaile on 5/18/2016.
 */
public class AndroidUtils {

    public static float dpToPx(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }
}
