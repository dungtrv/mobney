package ltvqt.com.mobney.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ltvqt.com.mobney.R;
import ltvqt.com.mobney.customview.PieChart;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link DashboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardFragment extends Fragment implements View.OnClickListener {

    private OnDashboardFragmentInteractionListener mListener;
    private Button btnAddgudget;

    public DashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DashboardFragment.
     */
    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.getMenu().clear();

        View view = getView();

        // TODO : mock data source
        PieChart.PieChartData[] dataSource = new PieChart.PieChartData[4];
        dataSource[0] = new PieChart.PieChartData();
        dataSource[0].setAngle(45);
        dataSource[0].setLabel("Clothing");
        dataSource[1] = new PieChart.PieChartData();
        dataSource[1].setAngle(90);
        dataSource[1].setLabel("Mobile Phone");
        dataSource[2] = new PieChart.PieChartData();
        dataSource[2].setAngle(190);
        dataSource[2].setLabel("Drinking");
        dataSource[3] = new PieChart.PieChartData();
        dataSource[3].setAngle(35);
        dataSource[3].setLabel("Sport");

        if (view != null) {
            PieChart pieChart = (PieChart) getView().findViewById(R.id.pie_chart);
            pieChart.setDataSource(dataSource);
        }

        btnAddgudget = (Button) view.findViewById(R.id.button_add_budget);

        btnAddgudget.setOnClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onDashboardFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDashboardFragmentInteractionListener) {
            mListener = (OnDashboardFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChartFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_add_budget: {

                AddbudgetFragment addbudgetFragment = new AddbudgetFragment();
                addbudgetFragment.show(getFragmentManager(), "");

                break;
            }
        }
    }

    public interface OnDashboardFragmentInteractionListener {

        void onDashboardFragmentInteraction(Uri uri);
    }
}
