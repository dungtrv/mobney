package ltvqt.com.mobney.fragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ltvqt.com.mobney.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddbudgetFragment extends DialogFragment {


    public AddbudgetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().setTitle(R.string.title_fragment_addbudget);

        return inflater.inflate(R.layout.fragment_addbudget, container, false);
    }

}
