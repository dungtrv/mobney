package ltvqt.com.mobney.model;

import java.util.ArrayList;

/**
 * Created by hoaile on 5/21/2016.
 */
public class TransactionModel {

    private String date;
    private ArrayList<TransactionDetail> details;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<TransactionDetail> getDetails() {
        return details;
    }

    public void setDetails(ArrayList<TransactionDetail> details) {
        this.details = details;
    }


    public static class TransactionDetail {

        private String title;
        private String amount;
        private boolean isPaid;
        private String categoryName;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public boolean isPaid() {
            return isPaid;
        }

        public void setPaid(boolean paid) {
            isPaid = paid;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }
    }
}
