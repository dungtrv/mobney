package ltvqt.com.mobney.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ltvqt.com.mobney.R;
import ltvqt.com.mobney.customview.LineChart;
import ltvqt.com.mobney.customview.PieChart;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnChartFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChartFragment extends Fragment implements Toolbar.OnMenuItemClickListener {

    private OnChartFragmentInteractionListener mListener;

    public ChartFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ChartFragment.
     */
    public static ChartFragment newInstance() {
        ChartFragment fragment = new ChartFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        View view = getView();

        // TODO : mock data source
        PieChart.PieChartData[] dataSource = new PieChart.PieChartData[4];
        dataSource[0] = new PieChart.PieChartData();
        dataSource[0].setAngle(45);
        dataSource[0].setLabel("Clothing");
        dataSource[1] = new PieChart.PieChartData();
        dataSource[1].setAngle(90);
        dataSource[1].setLabel("Mobile Phone");
        dataSource[2] = new PieChart.PieChartData();
        dataSource[2].setAngle(190);
        dataSource[2].setLabel("Drinking");
        dataSource[3] = new PieChart.PieChartData();
        dataSource[3].setAngle(35);
        dataSource[3].setLabel("Sport");

        if (view != null) {
            PieChart pieChart = (PieChart) getView().findViewById(R.id.chart_pie_chart);
            pieChart.setDataSource(dataSource);
        }


        // TODO : Mock data
        float[] data = new float[]{
                200, 100, 180, 330, 435, 123, 231, 12, 324, 0, 134, 222, 432, 71, 190, 246, 431, 111, 222, 333, 444, 322, 213, 131, 222, 111, 115, 23, 56, 332
        };

        if (view != null) {
            LineChart lineChart = (LineChart) view.findViewById(R.id.chart_line_chart);
            lineChart.setDataSource(data);
        }

        // hide title on action bar
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Chart - May");

        // create menu icon for tool bar
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.getMenu().clear();
        toolbar.inflateMenu(R.menu.chart_menu);
        toolbar.setOnMenuItemClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chart, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onChartFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChartFragmentInteractionListener) {
            mListener = (OnChartFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChartFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_filter: {

                AccountPopupFragment accountPopupFragment = AccountPopupFragment.newInstance();
                accountPopupFragment.show(getFragmentManager(), "");

                return true;
            }
            case R.id.action_calendar: {

                CalendarPopupFragment calendarPopupFragment = CalendarPopupFragment.newInstance();
                calendarPopupFragment.show(getFragmentManager(), "");

                return true;
            }
        }

        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnChartFragmentInteractionListener {
        // TODO: Update argument type and name
        void onChartFragmentInteraction(Uri uri);
    }
}
