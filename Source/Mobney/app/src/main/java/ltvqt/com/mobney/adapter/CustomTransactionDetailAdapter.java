package ltvqt.com.mobney.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ltvqt.com.mobney.R;
import ltvqt.com.mobney.model.TransactionModel;

/**
 * Created by hoaile on 5/21/2016.
 */
public class CustomTransactionDetailAdapter extends ArrayAdapter<TransactionModel.TransactionDetail> {

    Activity context;
    int resource;
    List<TransactionModel.TransactionDetail> data;

    public CustomTransactionDetailAdapter(Activity context, int resource, List<TransactionModel.TransactionDetail> objects) {
        super(context, resource, objects);
        this.context = context;
        this.data = objects;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        convertView = inflater.inflate(R.layout.transaction_list_detail_item, parent, false);

        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_transaction_detail_title);
        TextView tvAmount = (TextView) convertView.findViewById(R.id.tv_transaction_detail_amount);
        TextView tvCategory = (TextView) convertView.findViewById(R.id.tv_transaction_detail_category);
        TextView tvStatus = (TextView) convertView.findViewById(R.id.tv_transaction_detail_status);

        TransactionModel.TransactionDetail transactionDetail = data.get(position);

        tvTitle.setText(transactionDetail.getTitle());

        tvAmount.setText(transactionDetail.getAmount());

        if (transactionDetail.isPaid()) {

            tvAmount.setTextColor(Color.parseColor("#ffff4444"));
            tvStatus.setText(R.string.transaction_list_detail_status_paid);
        } else {
            tvAmount.setTextColor(Color.parseColor("#4caf50"));
            tvStatus.setText(R.string.transaction_list_detail_status_receive);
        }

        tvCategory.setText(transactionDetail.getCategoryName());

        return convertView;
    }
}
