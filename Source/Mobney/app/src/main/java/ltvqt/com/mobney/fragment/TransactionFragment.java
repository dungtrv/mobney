package ltvqt.com.mobney.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import ltvqt.com.mobney.R;
import ltvqt.com.mobney.adapter.CustomTransactionAdapter;
import ltvqt.com.mobney.model.TransactionModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnTransactionFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TransactionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TransactionFragment extends Fragment {

    private OnTransactionFragmentInteractionListener mListener;

    public TransactionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TransactionFragment.
     */
    public static TransactionFragment newInstance() {
        TransactionFragment fragment = new TransactionFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Transaction");

        View view = getView();
        if (view != null) {

            ListView lvTransaction = (ListView) view.findViewById(R.id.list_view_transaction);

            // TODO : Mock data
            ArrayList<TransactionModel> data = new ArrayList<>();

            TransactionModel m1 = new TransactionModel();
            m1.setDate("Today");

            ArrayList<TransactionModel.TransactionDetail> details1 = new ArrayList<>();

            TransactionModel.TransactionDetail d1 = new TransactionModel.TransactionDetail();
            d1.setAmount("$ 3000");
            d1.setCategoryName("Clothing");
            d1.setPaid(true);
            d1.setTitle("Mua ao moi");
            details1.add(d1);

            TransactionModel.TransactionDetail d2 = new TransactionModel.TransactionDetail();
            d2.setAmount("$ 3000");
            d2.setCategoryName("Clothing");
            d2.setPaid(false);
            d2.setTitle("Mua ao moi1");
            details1.add(d2);

            TransactionModel.TransactionDetail dx = new TransactionModel.TransactionDetail();
            dx.setAmount("$ 3000");
            dx.setCategoryName("Clothing");
            dx.setPaid(false);
            dx.setTitle("Mua ao moi1");
            details1.add(dx);

            m1.setDetails(details1);

            data.add(m1);

            // ==========

            TransactionModel m2 = new TransactionModel();
            m2.setDate("2016-05-04");

            ArrayList<TransactionModel.TransactionDetail> details2 = new ArrayList<>();

            TransactionModel.TransactionDetail d3 = new TransactionModel.TransactionDetail();
            d3.setAmount("$ 3000");
            d3.setCategoryName("Clothing");
            d3.setPaid(false);
            d3.setTitle("Mua ao moi11");
            details2.add(d3);

            TransactionModel.TransactionDetail d4 = new TransactionModel.TransactionDetail();
            d4.setAmount("$ 3000");
            d4.setCategoryName("Clothing");
            d4.setPaid(false);
            d4.setTitle("Mua ao moi");
            details2.add(d4);

            m2.setDetails(details2);

            data.add(m2);

            CustomTransactionAdapter adapter = new CustomTransactionAdapter(getActivity(), R.layout.transaction_list_item, data);
            lvTransaction.setAdapter(adapter);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onTransactionFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTransactionFragmentInteractionListener) {
            mListener = (OnTransactionFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChartFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnTransactionFragmentInteractionListener {

        void onTransactionFragmentInteraction(Uri uri);
    }
}
