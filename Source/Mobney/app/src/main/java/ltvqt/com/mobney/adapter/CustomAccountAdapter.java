package ltvqt.com.mobney.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ltvqt.com.mobney.R;
import ltvqt.com.mobney.model.AccountModel;

/**
 * Created by hoaile on 5/19/2016.
 */
public class CustomAccountAdapter extends ArrayAdapter<AccountModel> {

    Activity context = null;
    ArrayList<AccountModel> data = null;
    int layoutId;

    public CustomAccountAdapter(Activity context, int layoutId, ArrayList<AccountModel> arr) {

        super(context, layoutId, arr);

        this.context = context;
        this.layoutId = layoutId;
        this.data = arr;
    }

    @Override
    public int getCount() {

        return data.size();
    }

    @Override
    public AccountModel getItem(int position) {

        return data.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(layoutId, null);
        }

        AccountModel acc = getItem(position);

        // find text view account name
        TextView tvAccountName = (TextView) convertView.findViewById(R.id.tv_acc_list_acc_name);
        tvAccountName.setText(acc.getAccountName());

        // find text view balance
        TextView tvBalance = (TextView) convertView.findViewById(R.id.tv_acc_list_acc_balance);
        tvBalance.setText("$ " + String.valueOf(acc.getBalance()));

        if (acc.getBalance() < 0.0) {

            tvBalance.setTextColor(Color.parseColor("#f44336"));
        } else {
            tvBalance.setTextColor(Color.parseColor("#4caf50"));
        }

        return convertView;
    }
}
