package ltvqt.com.mobney.utils;

/**
 * Created by hoaile on 5/20/2016.
 */
public class CalculateUtils {

    public static float getMax(float[] arr) {

        float max = arr[0];
        for (float a : arr) {

            if (a > max) max = a;
        }

        return max;
    }

    /**
     * Get range value between max -> 0
     *
     * @param max
     * @return
     */
    public static int getRange(float max) {

        String str = String.valueOf((int) max);

        int unit = (int) Math.pow(10, str.length() - 1);

        return unit;
    }
}
