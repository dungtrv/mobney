package ltvqt.com.mobney.model;

/**
 * Created by hoaile on 5/19/2016.
 */
public class AccountModel {

    private String accountName;
    private float balance;


    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }
}
