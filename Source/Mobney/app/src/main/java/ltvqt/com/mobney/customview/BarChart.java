package ltvqt.com.mobney.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import ltvqt.com.mobney.utils.AndroidUtils;
import ltvqt.com.mobney.utils.CalculateUtils;

/**
 * Created by hoaile on 5/20/2016.
 */
public class BarChart extends View {

    final String[] colorArr = new String[]{"#c6ff8c", "#fff78c", "#ffd38c", "#8cebff", "#ff8e9c"};

    public BarChart(Context context) {

        super(context);
    }

    public BarChart(Context context, AttributeSet attrs) {

        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);

        // TODO : Mock data
        float[] data = new float[]{
                200, 100, 180, 330, 435, 123, 231, 12, 324, 0, 134, 222
        };

        float maxY = CalculateUtils.getMax(data);
        // draw axes X, Y
        final int width = getWidth();
        final int height = getHeight();

        final float axeWidth = 2;

        final float labelWidth = AndroidUtils.dpToPx(getContext(), 50);

        Paint axePaint = new Paint();
        axePaint.setStrokeWidth(axeWidth);
        axePaint.setTextSize(36);

//        canvas.drawLine(0, 0, 0, height, axePaint);
//        canvas.drawLine(0, height, width, height, axePaint);

        // draw Y axist
        final int range = CalculateUtils.getRange(maxY);

        float startY = 0;
        while (startY < maxY) {

            float y = height - (startY / maxY) * height - axeWidth;

            axePaint.setColor(Color.parseColor("#cccccc"));
            canvas.drawLine(labelWidth, y, width, y, axePaint);

            axePaint.setColor(Color.GRAY);
            canvas.drawText(String.valueOf(startY), 0, y, axePaint);
            startY += range;
        }

        // draw chart

        final float chartDistance = AndroidUtils.dpToPx(getContext(), 5);
        float startX = labelWidth;
        float chartWidth = (width - axeWidth - labelWidth) / data.length;
        int colorIndex = 0;

        Paint chartPaint = new Paint();

        for (float d : data) {

            chartPaint.setColor(Color.parseColor(colorArr[colorIndex]));

            float y = height - (d / maxY) * (height - axeWidth) - axeWidth;

            canvas.drawRect(startX, y, startX + chartWidth - chartDistance, height, chartPaint);

            startX += chartWidth;

            colorIndex++;
            if (colorIndex == colorArr.length) colorIndex = 0;
        }

    }
}
