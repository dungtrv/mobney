package ltvqt.com.mobney.fragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import ltvqt.com.mobney.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AccountPopupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountPopupFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public AccountPopupFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AccountPopupFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AccountPopupFragment newInstance() {
        AccountPopupFragment fragment = new AccountPopupFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        getDialog().setTitle(R.string.title_fragment_select_account);

        return inflater.inflate(R.layout.fragment_account_popup, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        View view = getView();

        if (view != null) {

            // TODO : mock data
            ArrayList<CharSequence> data = new ArrayList<>();
            data.add("Le Hoai");
            data.add("Le Hung");

            AppCompatSpinner spinner = (AppCompatSpinner) view.findViewById(R.id.sprinner_account);

            ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, data);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinner.setAdapter(adapter);
        }
    }

}
