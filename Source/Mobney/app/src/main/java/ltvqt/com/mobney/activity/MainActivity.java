package ltvqt.com.mobney.activity;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;

import ltvqt.com.mobney.R;
import ltvqt.com.mobney.fragment.AccountFragment;
import ltvqt.com.mobney.fragment.CategoryFragment;
import ltvqt.com.mobney.fragment.ChartFragment;
import ltvqt.com.mobney.fragment.DashboardFragment;
import ltvqt.com.mobney.fragment.SettingFragment;
import ltvqt.com.mobney.fragment.TransactionFragment;
import ltvqt.com.mobney.utils.AndroidUtils;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        DashboardFragment.OnDashboardFragmentInteractionListener,
        AccountFragment.OnAccountFragmentInteractionListener,
        CategoryFragment.OnCategoryFragmentInteractionListener,
        SettingFragment.OnSettingFragmentInteractionListener,
        TransactionFragment.OnTransactionFragmentInteractionListener,
        ChartFragment.OnChartFragmentInteractionListener,
        View.OnClickListener {

    private Animation rotate_forward, rotate_backward;
    private FloatingActionButton fab, fabIncome, fabOutcome, fabAccount, fabCategory;
    private LinearLayout famOutcome, famIncome, famAccount, famCategory, bgFloatButton;

    private boolean[] FABVisible = new boolean[]{true, true, true, true};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                animateFAB();
            }
        });

        fabIncome = (FloatingActionButton) findViewById(R.id.fab_transaction_income);
        fabOutcome = (FloatingActionButton) findViewById(R.id.fab_transaction_outcome);
        fabAccount = (FloatingActionButton) findViewById(R.id.fab_account);
        fabCategory = (FloatingActionButton) findViewById(R.id.fab_category);


        famOutcome = (LinearLayout) findViewById(R.id.fam_outcome);
        famIncome = (LinearLayout) findViewById(R.id.fam_income);
        famAccount = (LinearLayout) findViewById(R.id.fam_account);
        famCategory = (LinearLayout) findViewById(R.id.fam_category);
        bgFloatButton = (LinearLayout) findViewById(R.id.bg_float_action_button);

        famAccount.setOnClickListener(this);
        famCategory.setOnClickListener(this);
        famIncome.setOnClickListener(this);
        famOutcome.setOnClickListener(this);

        bgFloatButton.setOnClickListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // set selected menu
        navigationView.getMenu().getItem(0).setChecked(true);

        // set default fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment, DashboardFragment.newInstance());
        fragmentTransaction.commit();

        // animation for floating button
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (isFabOpen) {

            hideFloatButton();
        } else {

            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        hideFloatButton();

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        Fragment fr = null;

        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {

            fr = DashboardFragment.newInstance();

            fab.setVisibility(View.VISIBLE);

            FABVisible[0] = true;
            FABVisible[1] = true;
            FABVisible[2] = true;
            FABVisible[3] = true;

        } else if (id == R.id.nav_transaction) {

            fr = TransactionFragment.newInstance();

            fab.setVisibility(View.VISIBLE);

            FABVisible[0] = true;
            FABVisible[1] = true;
            FABVisible[2] = false;
            FABVisible[3] = false;

        } else if (id == R.id.nav_chart) {

            fr = ChartFragment.newInstance();

            fab.setVisibility(View.INVISIBLE);

            FABVisible[0] = false;
            FABVisible[1] = false;
            FABVisible[2] = false;
            FABVisible[3] = false;

        } else if (id == R.id.nav_account) {

            fr = AccountFragment.newInstance();

            fab.setVisibility(View.VISIBLE);

            FABVisible[0] = false;
            FABVisible[1] = false;
            FABVisible[2] = true;
            FABVisible[3] = false;

        } else if (id == R.id.nav_category) {

            fr = CategoryFragment.newInstance();

            fab.setVisibility(View.VISIBLE);

            FABVisible[0] = false;
            FABVisible[1] = false;
            FABVisible[2] = false;
            FABVisible[3] = true;

        } else if (id == R.id.nav_setting) {

            fr = SettingFragment.newInstance();

            fab.setVisibility(View.INVISIBLE);

            FABVisible[0] = false;
            FABVisible[1] = false;
            FABVisible[2] = false;
            FABVisible[3] = false;
        }

        fragmentTransaction.replace(R.id.main_fragment, fr);
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onChartFragmentInteraction(Uri uri) {

    }

    @Override
    public void onDashboardFragmentInteraction(Uri uri) {


    }

    @Override
    public void onAccountFragmentInteraction(Uri uri) {


    }

    @Override
    public void onCategoryFragmentInteraction(Uri uri) {


    }

    @Override
    public void onSettingFragmentInteraction(Uri uri) {


    }

    @Override
    public void onTransactionFragmentInteraction(Uri uri) {

    }

    private boolean isFabOpen = false;

    private void animateFAB() {

        if (isFabOpen) {

            hideFloatButton();
        } else {

            showFloatButton();
        }
    }

    private void hideFloatButton() {

        if (isFabOpen) {
            float startY = -70;

            fab.startAnimation(rotate_backward);

            if (FABVisible[3]) {
                buildTranslateBotAnimation(startY, famCategory);
                startY -= 70;
            }

            if (FABVisible[2]) {
                buildTranslateBotAnimation(startY, famAccount);
                startY -= 70;
            }

            if (FABVisible[1]) {
                buildTranslateBotAnimation(startY, famIncome);
                startY -= 70;
            }

            if (FABVisible[0]) {
                buildTranslateBotAnimation(startY, famOutcome);
            }

            isFabOpen = false;
            bgFloatButton.setVisibility(View.INVISIBLE);
        }
    }

    private void showFloatButton() {

        // calculate position of float button
        float startY = -70;

        fab.startAnimation(rotate_forward);

        if (FABVisible[3]) {
            buildTranslateTopAnimation(startY, famCategory);
            startY -= 70;
        }

        if (FABVisible[2]) {
            buildTranslateTopAnimation(startY, famAccount);
            startY -= 70;
        }

        if (FABVisible[1]) {
            buildTranslateTopAnimation(startY, famIncome);
            startY -= 70;
        }

        if (FABVisible[0]) {
            buildTranslateTopAnimation(startY, famOutcome);
        }

        isFabOpen = true;
        bgFloatButton.setVisibility(View.VISIBLE);
    }

    /**
     * Build translate animation for these float buttons
     *
     * @param top (dp)
     * @return
     */
    private void buildTranslateTopAnimation(float top, final View v) {

        final float padding = 50;

        final float realTop = AndroidUtils.dpToPx(this, top) - padding;

        ObjectAnimator o = ObjectAnimator.ofFloat(v, "translationY", 0f, realTop);
        o.setDuration(200);
        o.setInterpolator(new LinearInterpolator());

        v.setVisibility(View.VISIBLE);
        o.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {


                ObjectAnimator o = ObjectAnimator.ofFloat(v, "translationY", realTop, realTop + padding);
                o.setDuration(200);
                o.setInterpolator(new LinearInterpolator());
                o.start();

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        o.start();
    }

    private void buildTranslateBotAnimation(float top, final View v) {

        ObjectAnimator o = ObjectAnimator.ofFloat(v, "translationY", AndroidUtils.dpToPx(this, top), 0f);
        o.setDuration(200);
        o.setInterpolator(new LinearInterpolator());

        o.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

                v.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        o.start();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.bg_float_action_button: {

                hideFloatButton();
                break;
            }
            case R.id.fam_account: {

                hideFloatButton();
                Intent intent = new Intent(v.getContext(), AccountActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.fam_category: {

                hideFloatButton();
                Intent intent = new Intent(v.getContext(), CategoryActivity.class);
                startActivity(intent);
                break;
            }

            case R.id.fam_income: {

                hideFloatButton();
                Intent intent = new Intent(v.getContext(), TransactionActivity.class);
                intent.putExtra(TransactionActivity.BUNDLE_PAID_TAG, false);

                startActivity(intent);

                break;
            }

            case R.id.fam_outcome: {

                hideFloatButton();
                Intent intent = new Intent(v.getContext(), TransactionActivity.class);
                intent.putExtra(TransactionActivity.BUNDLE_PAID_TAG, true);

                startActivity(intent);

                break;
            }
        }
    }
}
